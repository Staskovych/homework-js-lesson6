
function Human (name) {
  this.name = name;
  this.add = function () {
    Human.counter++;
  }
  this.add();
}

Human.counter = 0;

let number1 = new Human('number 1');
let number2 = new Human('number 2');
let number3 = new Human('number 3');

console.log(Human.counter);



