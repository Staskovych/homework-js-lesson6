
function Human (age, name, country) {
  this.age = age
  this.name = name
  this.country = country
};

function sortByAge (arr) {
  arr.sort( (a, b) => a.age > b.age ? -1 :  1 );
}

let hmn1 = {name: 'Paulas', age: 12, country: 'Brazil'};
let hmn2 = {name: 'Petro', age: 25, country: 'Ukraine'};
let hmn3 = {name: 'Vilyam', age: 19, country: "Usa"};

const humans = [hmn1, hmn2, hmn3];

sortByAge(humans);

console.log(humans);





